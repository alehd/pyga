# PyGA

Simple pure-python Genetic Algorithm python library.

## Related work

This is an extremely simple API used for quick prototyping. Mature gradient-free optimization implementations include:

- https://github.com/CMA-ES/pycma

- https://github.com/SimonBlanke/Gradient-Free-Optimizers

- https://github.com/hardmaru/estool/

- https://github.com/trevorstephens/gplearn

- https://github.com/MilesCranmer/PySR

## Installation

Install using your usual package manager or simply download [pyga/genetic.py](pyga/genetic.py).

```
# With pip
python -m pip install git+https://gitlab.com/da_doomer/pyga

# With poetry
poetry add git+https://gitlab.com/da_doomer/pyga

# Without package manager
curl https://gitlab.com/da_doomer/pyga/-/blob/master/pyga/pyga.py
```

## Usage

Pyga is only a blue-print, so each user implements the Genetic Algorithm operators for their use case and uses Pyga for the generic logic.

```
import pyga

# Define operators...
def init():
	pass

def mutated(genotype):
	pass

def fitness(genotype):
	pass

def main():
	options = {
		"population_size": 300,
		# ...
	}
	ga = pyga.GeneticAlgorithm(init, mutated, **options)

	# Optimize
	while not ga.finished:
		# Ask for solutions
		solutions = ga.ask()

		# Evaluate solutions with the executor
		values = map(fitness, solutions)

		# Tell the solver the solution values
		ga.tell(solutions, values)

		# Display information
		ga.disp()

	# Log the optimization process
	fig_file = "ga.svg"
	fig = ga.plot_fig()
	fig.savefig(fig_file)

	# Return the solution
	return ga.elite

if __name__ == "__main__":
	main()
```
